
Pod::Spec.new do |s|

  s.name         = "YHManager"
  s.version      = "1.2.0"
  s.summary      = "YHManager is only a test sdk"

  s.description  = "YHManager is only a test sdk, you can have a test for sdk"

  s.homepage     = "https://gitee.com/lang123321/YHManager"

  s.license      = "MIT "

  s.author       = { "zhengxiaolang" => "haifeng3099@126.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/lang123321/YHManager.git", :tag => "1.2.0" }

  s.source_files  = "Classes", "Classes/*"

  s.requires_arc = true

  s.vendored_frameworks = ['Classes/*.framework']
  
end
